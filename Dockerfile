FROM nginx:1.18.0-alpine

# add a configuration file
COPY nginx.conf /etc/nginx/nginx.conf

# add volumes 